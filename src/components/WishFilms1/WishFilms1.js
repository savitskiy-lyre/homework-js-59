import React, {Component} from 'react';
import Film from "./Film/Film";
import FilmForm from "./FilmForm/FilmForm";

class WishFilms1 extends Component {

   constructor(props) {
      super(props);
      const storageItem = localStorage.getItem('wishFilms');
      if (storageItem) {
         this.state = {wishFilms: JSON.parse(storageItem)};
      } else {
         this.state = {
            wishFilms: [],
         }
      }
   }

   static getDerivedStateFromProps(props, state) {
      if (state.wishFilms.length > 0) {
         localStorage.setItem('wishFilms', JSON.stringify(state.wishFilms));
      }
      return null;
   }

   addNewFilm = (text) => {
      this.setState((prev) => {
         return {
            wishFilms: [...prev.wishFilms, {text: text, id: Math.random()}],
         }
      });
   };

   removeFilm = (id,) => {
      for (let i = 0; i < this.state.wishFilms.length; i++) {
         if (id === this.state.wishFilms[i]['id']) {
            this.setState(() => {
               const updatedArr = [...this.state.wishFilms];
               updatedArr.splice(i, 1)
               if (updatedArr.length === 0) {
                  localStorage.removeItem('wishFilms');
               }
               return {wishFilms: updatedArr};
            })
         }
      }
   };

   onInputChange = (id, e) => {
      for (let i = 0; i < this.state.wishFilms.length; i++) {
         if (id === this.state.wishFilms[i]['id']) {
            this.setState(() => {
               const updatedArr = [...this.state.wishFilms];
               updatedArr[i]['text'] = e.target.value;
               return {wishFilms: updatedArr};
            })
         }
      }
   };

   render() {
      return (
        <div>
           <FilmForm addNewFilm={this.addNewFilm}/>
           {this.state.wishFilms.map((film) => {
              return (
                <Film
                  text={film.text}
                  key={film.id}
                  removeFilm={() => this.removeFilm(film.id)}
                  onInputChange={(e) => this.onInputChange(film.id, e)}
                />)
           })}
        </div>
      );
   };
}

export default WishFilms1;