import React from 'react';

const Film = ({onInputChange, text, removeFilm}) => {
   return (
     <div className="film-wrapper">
        <input type="text" onChange={onInputChange} value={text}/>
        <button onClick={removeFilm}>X</button>
     </div>
   );
};

export default /*Film*/ React.memo(Film, (prev, next) => prev.text === next.text);