import React, {useEffect, useState} from 'react';
import FilmForm from "./FilmForm/FilmForm";
import Film from "./Film/Film";

const WishFilms2 = () => {
   const [wishFilms, setWishFilms] = useState([])

   const addNewFilm = (text) => {
      setWishFilms([...wishFilms, {text: text, id: Math.random()}]);
   };

   const removeFilm = (id) => {
      setWishFilms((prev) => {
         const updatedState = [...prev];
         updatedState.forEach((film) => {
            if (id === film.id) {
               updatedState.splice(updatedState.indexOf(film),1);
            }
         });
         return updatedState;
      });
   };

   const onInputChange = (id, e) => {
      setWishFilms((prev) => {
         return prev.map((film) => {
            if (id === film.id) {
               return {...film, text: e.target.value};
            }
            return film;
         });
      });
   };
   useEffect(() => {
      if (localStorage.getItem('wishFilms2')){
         setWishFilms(JSON.parse(localStorage.getItem('wishFilms2')));
      }
   }, [])
   useEffect(() => {
      if (wishFilms.length > 0){
         localStorage.setItem('wishFilms2', JSON.stringify(wishFilms));
      }
   },[wishFilms])
   return (
     <div>
        <FilmForm addNewFilm={addNewFilm}/>
        {wishFilms.map((film) => {
           return (
             <Film
               text={film.text}
               key={film.id}
               removeFilm={() => removeFilm(film.id)}
               onInputChange={(e) => onInputChange(film.id, e)}
             />)
        })}
     </div>
   );
};

export default WishFilms2;