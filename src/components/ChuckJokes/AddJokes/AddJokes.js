import React, {PureComponent} from 'react';

class AddJokes extends PureComponent {
   render() {
      return (
        <div>
           <label>Count of jokes: <input type="number" onChange={this.props.onChangeCount} value={this.props.count}/></label>
           <button onClick={this.props.requestNewJokes}>New jokes please</button>
        </div>
      );
   }
}

export default AddJokes;