import React from 'react';
import axios from "axios";
import AddJokes from "./AddJokes/AddJokes";
import Joke from "./Joke/Joke";
import './ChuckJokes.css';

class ChuckJokes extends React.Component {
   state = {
      jokes: [],
      isRequestBeen: false,
      count: '',
   }

   requestNewJokes = async () => {
      const responeArr = [];
      const count = this.state.count ? this.state.count : 1;
      this.setState({isRequestBeen: true});
      for (let i = 0; i < count; i++) {
         responeArr.push(axios.get('https://api.chucknorris.io/jokes/random'));
      }

      const responses = await Promise.all(responeArr);
      this.setState(() => {
         return {
            jokes: responses.map((joke) => joke.data),
            isRequestBeen: false,
         };
      })
   };

   static getDerivedStateFromProps(props, state) {
      if (state.isRequestBeen) {
         return {...state, jokes: []};
      }
      return null;
   }

   onChangeCount = (e) => {
      let value = parseInt(e.target.value);
      if (e.target.value === '') {
         this.setState({count: e.target.value});
      } else if (isNaN(value) || value < 1) {
         return null;
      } else {
         this.setState({count: value});
      }

   }

   render() {
      return (
        <div className="ChuckJokes-wrapper">
           <AddJokes onChangeCount={this.onChangeCount}
                     count={this.state.count}
                     requestNewJokes={this.requestNewJokes}/>
           <div>
              {this.state.jokes.length !== 0 ? this.state.jokes.map((joke) => {
                 return (<Joke key={joke.id} text={joke.value}/>)
              }) : null}
              {this.state.isRequestBeen ? <div className='preloader'/> : null}
           </div>

        </div>
      );
   }
}

export default ChuckJokes;