import './App.css';
import React from 'react';
import WishFilms1 from "./components/WishFilms1/WishFilms1";
import WishFilms2 from "./components/WishFilms2/WishFilms2";
import ChuckJokes from "./components/ChuckJokes/ChuckJokes";


function App() {

   return (
     <div className="App">
        <WishFilms1/>
        <ChuckJokes/>
        <WishFilms2/>
     </div>
   );
}

export default App;